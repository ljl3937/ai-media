import praw
import time
import os

from langchain.agents import Tool
from langchain.tools import tool
from crewai import Agent, Task, Process, Crew
from langchain_community.utilities import GoogleSerperAPIWrapper

from myllms import *
from tools.csdn import BrowserTool
from crewai_tools import WebsiteSearchTool

# To Load GPT-4
api = os.environ.get("OPENAI_API_KEY")

llm = MyLLM(model_name="qianfan").get_llm()


from langchain.agents import load_tools

# To load Human in the loop
human_tools = load_tools(["human"])

# search = GoogleSerperAPIWrapper()

# search_tool = Tool(
#     name="Scrape google searches",
#     func=search.run,
#     description="useful for when you need to ask the agent to search the internet",
# )

csdn_search = BrowserTool().search
view_article = BrowserTool().view_article
web_tool = WebsiteSearchTool()


"""
- define agents that are going to research latest AI tools and write a blog about it 
- explorer will use access to internet and LocalLLama subreddit to get all the latest news
- writer will write drafts 
- critique will provide feedback and make sure that the blog text is engaging and easy to understand
"""

fenxi = Agent(
    role="问题分析师",
    goal="接收用户输入的问题，并进行分析以确定文章的主题和方向。",
    backstory="""使用自然语言处理技术来提取问题的关键词和意图，根据这些信息从预设的标签库中匹配问题类别。
    以知乎回答的风格和水平为标准，确定文章的主题和方向，并列出写作大纲。
    """,
    verbose=True,
    allow_delegation=True,
    # llm=baichuan,  # 移除以使用默认的gpt-4
    llm=llm,  # 移除以使用默认的gpt-4
)

searcher = Agent(
    role="资料搜集专家",
    goal="根据问题分析智能体的输出，找到合适的搜索词，根据标题选出前5篇参考资料，并访问其url,总结每篇文章的主要内容。",
    backstory="""从参考资料中检索与关键词匹配的内容，使用相关性算法进行排序。""",
    verbose=True,
    allow_delegation=True,
    tools=[csdn_search, web_tool],
    llm=llm,  # remove to use default gpt-4
    function_calling_llm="gpt-4",
)
writer = Agent(
    role="知乎文章作家",
    goal="利用前面问题分析师列出的文章大纲和搜集到的资料，按照特定知乎爆文的风格生成文章。",
    backstory="""选择最相关的案例和故事，结合问题分析的结果，使用预设的文章结构模板生成初稿。
    """,
    verbose=True,
    allow_delegation=True,
    memory=True,
    llm=llm,  # remove to use default gpt-4
)
reviewer = Agent(
    role="知乎文章优化专家",
    goal="审查和优化文章内容，确保符合知乎爆文的标准，并输出最终的文章。",
    backstory="""运用语言模型对文章进行润色，移除冗余内容，增强逻辑性和说服力，符合知乎爆文的要求。
    """,
    verbose=True,
    allow_delegation=False,
    memory=True,
    llm=llm,  # remove to use default gpt-4
)
user_question = input("请输入知乎问题：")

task_fenxi = Task(
    description=f"""知乎问题：{user_question} 请据此进行分析以确定文章的主题和方向。""",
    agent=fenxi,
)

task_search = Task(
    description="""根据问题分析智能体的输出，搜集相关的参考资料，并总结每篇文章的主要内容。将搜索结果返回给问题分析智能体。""",
    agent=searcher,
)

task_write = Task(
    description="""利用搜集到的资料，按照特定知乎爆文的风格生成文章。
    1、文章的主要脉络还是以问题分析师确定的文章大纲为主要脉络。
    2、用到的案例和故事要穿插到文章中，而不是简单的罗列""",
    agent=writer,
    context=[task_fenxi,task_search],
)

task_review = Task(
    description="""审查和优化文章内容，确保符合以下的要求。
    1、文章深度、易于理解，且使用通俗易懂的语言。
    2、务必符合知乎体的风格和水平。
    3、足够的吸引眼球，观点足够鲜明，能够制造冲突。
    4、排版整齐，没有错别字和语法错误。
    5、能使用案例的地方尽量使用案例，能使用故事的地方尽量使用故事。
    6、输出最终优化过的文章""",
    agent=reviewer,
    context=[task_write]
)

# instantiate crew of agents
crew = Crew(
    agents=[fenxi, searcher, writer, reviewer],
    tasks=[task_fenxi, task_search, task_write, task_review],
    verbose=2,
    process=Process.sequential,  # Sequential process will have tasks executed one after the other and the outcome of the previous one is passed as extra content into this next.
    max_rpm=12,
)

# Get your crew to work!
result = crew.kickoff()

print("######################")
print(result)