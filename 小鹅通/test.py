from Crypto.Cipher import AES
from urllib.request import urlopen

# 获取密钥
key_url = "https://app.xiaoe-tech.com/get_video_key.php?edk=CiCIWDxgGMcrT930tNYuKVLZuVJUwkLNEy%2BnIJhp4HdB%2BxCO08TAChiaoOvUBCokYjRhNjFiNTgtMmVhNy00OWYxLTgwZGMtZTE0NTIyODc5YWIy&fileId=5576678020536835056&keySource=VodBuildInKMS"
key = urlopen(key_url).read()

# 获取加密的TS文件
with open('ts/第三课：选平台/v.f421220_0_2385.ts', 'rb') as f:
    encrypted_ts = f.read()

# 创建一个新的AES对象
cipher = AES.new(key, AES.MODE_CBC, IV=b'\x00'*16)

# 解密TS文件
decrypted_ts = cipher.decrypt(encrypted_ts)

# 写入新的TS文件
with open('decrypted.ts', 'wb') as f:
    f.write(decrypted_ts)