const crypto = require('crypto');

var zy;
!function xx(t){
    var e = {};
    function r(i) {
        if (e[i])
            return e[i].exports;
        var n = e[i] = {
            i: i,
            l: !1,
            exports: {}
        };
        return t[i].call(n.exports, n, n.exports, r),
        n.l = !0,
        n.exports
    }
    return r.m = t,
                r.c = e,
                r.d = function(t, e, i) {
                    r.o(t, e) || Object.defineProperty(t, e, {
                        enumerable: !0,
                        get: i
                    })
                }
                ,
                r.r = function(t) {
                    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
                        value: "Module"
                    }),
                    Object.defineProperty(t, "__esModule", {
                        value: !0
                    })
                }
                ,
                r.t = function(t, e) {
                    if (1 & e && (t = r(t)),
                    8 & e)
                        return t;
                    if (4 & e && "object" == typeof t && t && t.__esModule)
                        return t;
                    var i = Object.create(null);
                    if (r.r(i),
                    Object.defineProperty(i, "default", {
                        enumerable: !0,
                        value: t
                    }),
                    2 & e && "string" != typeof t)
                        for (var n in t)
                            r.d(i, n, function(e) {
                                return t[e]
                            }
                            .bind(null, n));
                    return i
                }
                ,
                r.n = function(t) {
                    var e = t && t.__esModule ? function() {
                        return t.default
                    }
                    : function() {
                        return t
                    }
                    ;
                    return r.d(e, "a", e),
                    e
                }
                ,
                r.o = function(t, e) {
                    return Object.prototype.hasOwnProperty.call(t, e)
                }
                ,
                r.p = "",
    zy=r
}({
    "6de4": function(t, r, i) {
        "use strict";
        i.r(r),
        i.d(r, "version", (function() {
            return n
        }
        )),
        i.d(r, "VERSION", (function() {
            return s
        }
        )),
        i.d(r, "atob", (function() {
            return D
        }
        )),
        i.d(r, "atobPolyfill", (function() {
            return C
        }
        )),
        i.d(r, "btoa", (function() {
            return b
        }
        )),
        i.d(r, "btoaPolyfill", (function() {
            return y
        }
        )),
        i.d(r, "fromBase64", (function() {
            return N
        }
        )),
        i.d(r, "toBase64", (function() {
            return R
        }
        )),
        i.d(r, "utob", (function() {
            return w
        }
        )),
        i.d(r, "encode", (function() {
            return R
        }
        )),
        i.d(r, "encodeURI", (function() {
            return L
        }
        )),
        i.d(r, "encodeURL", (function() {
            return L
        }
        )),
        i.d(r, "btou", (function() {
            return I
        }
        )),
        i.d(r, "decode", (function() {
            return N
        }
        )),
        i.d(r, "isValid", (function() {
            return U
        }
        )),
        i.d(r, "fromUint8Array", (function() {
            return E
        }
        )),
        i.d(r, "toUint8Array", (function() {
            return O
        }
        )),
        i.d(r, "extendString", (function() {
            return G
        }
        )),
        i.d(r, "extendUint8Array", (function() {
            return j
        }
        )),
        i.d(r, "extendBuiltins", (function() {
            return H
        }
        )),
        i.d(r, "Base64", (function() {
            return K
        }
        ));
        const n = "3.7.5"
          , s = n
          , a = "function" == typeof atob
          , o = "function" == typeof btoa
          , l = "function" == typeof e
          , c = "function" == typeof TextDecoder ? new TextDecoder : void 0
          , u = "function" == typeof TextEncoder ? new TextEncoder : void 0
          , h = Array.prototype.slice.call("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=")
          , d = (t=>{
            let e = {};
            return t.forEach((t,r)=>e[t] = r),
            e
        }
        )(h)
          , f = /^(?:[A-Za-z\d+\/]{4})*?(?:[A-Za-z\d+\/]{2}(?:==)?|[A-Za-z\d+\/]{3}=?)?$/
          , p = String.fromCharCode.bind(String)
          , g = "function" == typeof Uint8Array.from ? Uint8Array.from.bind(Uint8Array) : t=>new Uint8Array(Array.prototype.slice.call(t, 0))
          , m = t=>t.replace(/=/g, "").replace(/[+\/]/g, t=>"+" == t ? "-" : "_")
          , v = t=>t.replace(/[^A-Za-z0-9\+\/]/g, "")
          , y = t=>{
            let e, r, i, n, s = "";
            const a = t.length % 3;
            for (let o = 0; o < t.length; ) {
                if ((r = t.charCodeAt(o++)) > 255 || (i = t.charCodeAt(o++)) > 255 || (n = t.charCodeAt(o++)) > 255)
                    throw new TypeError("invalid character found");
                e = r << 16 | i << 8 | n,
                s += h[e >> 18 & 63] + h[e >> 12 & 63] + h[e >> 6 & 63] + h[63 & e]
            }
            return a ? s.slice(0, a - 3) + "===".substring(a) : s
        }
          , b = o ? t=>btoa(t) : l ? t=>e.from(t, "binary").toString("base64") : y
          , T = l ? t=>e.from(t).toString("base64") : t=>{
            let e = [];
            for (let r = 0, i = t.length; r < i; r += 4096)
                e.push(p.apply(null, t.subarray(r, r + 4096)));
            return b(e.join(""))
        }
          , E = (t,e=!1)=>e ? m(T(t)) : T(t)
          , S = t=>{
            if (t.length < 2)
                return (e = t.charCodeAt(0)) < 128 ? t : e < 2048 ? p(192 | e >>> 6) + p(128 | 63 & e) : p(224 | e >>> 12 & 15) + p(128 | e >>> 6 & 63) + p(128 | 63 & e);
            var e = 65536 + 1024 * (t.charCodeAt(0) - 55296) + (t.charCodeAt(1) - 56320);
            return p(240 | e >>> 18 & 7) + p(128 | e >>> 12 & 63) + p(128 | e >>> 6 & 63) + p(128 | 63 & e)
        }
          , _ = /[\uD800-\uDBFF][\uDC00-\uDFFFF]|[^\x00-\x7F]/g
          , w = t=>t.replace(_, S)
          , A = l ? t=>e.from(t, "utf8").toString("base64") : u ? t=>T(u.encode(t)) : t=>b(w(t))
          , R = (t,e=!1)=>e ? m(A(t)) : A(t)
          , L = t=>R(t, !0)
          , k = /[\xC0-\xDF][\x80-\xBF]|[\xE0-\xEF][\x80-\xBF]{2}|[\xF0-\xF7][\x80-\xBF]{3}/g
          , x = t=>{
            switch (t.length) {
            case 4:
                var e = ((7 & t.charCodeAt(0)) << 18 | (63 & t.charCodeAt(1)) << 12 | (63 & t.charCodeAt(2)) << 6 | 63 & t.charCodeAt(3)) - 65536;
                return p(55296 + (e >>> 10)) + p(56320 + (1023 & e));
            case 3:
                return p((15 & t.charCodeAt(0)) << 12 | (63 & t.charCodeAt(1)) << 6 | 63 & t.charCodeAt(2));
            default:
                return p((31 & t.charCodeAt(0)) << 6 | 63 & t.charCodeAt(1))
            }
        }
          , I = t=>t.replace(k, x)
          , C = t=>{
            if (t = t.replace(/\s+/g, ""),
            !f.test(t))
                throw new TypeError("malformed base64.");
            t += "==".slice(2 - (3 & t.length));
            let e, r, i, n = "";
            for (let s = 0; s < t.length; )
                e = d[t.charAt(s++)] << 18 | d[t.charAt(s++)] << 12 | (r = d[t.charAt(s++)]) << 6 | (i = d[t.charAt(s++)]),
                n += 64 === r ? p(e >> 16 & 255) : 64 === i ? p(e >> 16 & 255, e >> 8 & 255) : p(e >> 16 & 255, e >> 8 & 255, 255 & e);
            return n
        }
          , D = a ? t=>atob(v(t)) : l ? t=>e.from(t, "base64").toString("binary") : C
          , P = l ? t=>g(e.from(t, "base64")) : t=>g(D(t).split("").map(t=>t.charCodeAt(0)))
          , O = t=>P(F(t))
          , M = l ? t=>e.from(t, "base64").toString("utf8") : c ? t=>c.decode(P(t)) : t=>I(D(t))
          , F = t=>v(t.replace(/[-_]/g, t=>"-" == t ? "+" : "/"))
          , N = t=>M(F(t))
          , U = t=>{
            if ("string" != typeof t)
                return !1;
            const e = t.replace(/\s+/g, "").replace(/={0,2}$/, "");
            return !/[^\s0-9a-zA-Z\+/]/.test(e) || !/[^\s0-9a-zA-Z\-_]/.test(e)
        }
          , B = t=>({
            value: t,
            enumerable: !1,
            writable: !0,
            configurable: !0
        })
          , G = function() {
            const t = (t,e)=>Object.defineProperty(String.prototype, t, B(e));
            t("fromBase64", (function() {
                return N(this)
            }
            )),
            t("toBase64", (function(t) {
                return R(this, t)
            }
            )),
            t("toBase64URI", (function() {
                return R(this, !0)
            }
            )),
            t("toBase64URL", (function() {
                return R(this, !0)
            }
            )),
            t("toUint8Array", (function() {
                return O(this)
            }
            ))
        }
          , j = function() {
            const t = (t,e)=>Object.defineProperty(Uint8Array.prototype, t, B(e));
            t("toBase64", (function(t) {
                return E(this, t)
            }
            )),
            t("toBase64URI", (function() {
                return E(this, !0)
            }
            )),
            t("toBase64URL", (function() {
                return E(this, !0)
            }
            ))
        }
          , H = ()=>{
            G(),
            j()
        }
          , K = {
            version: n,
            VERSION: s,
            atob: D,
            atobPolyfill: C,
            btoa: b,
            btoaPolyfill: y,
            fromBase64: N,
            toBase64: R,
            encode: R,
            encodeURI: L,
            encodeURL: L,
            utob: w,
            btou: I,
            decode: N,
            isValid: U,
            fromUint8Array: E,
            toUint8Array: O,
            extendString: G,
            extendUint8Array: j,
            extendBuiltins: H
        }
    },
})

P = zy("6de4")

function O(data) {
    const hash = crypto.createHash('md5');
    hash.update(data);
    return hash.digest('hex');
}
M = {
    encrypt: function(t) {
        var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "appbgzjnopv1917";
        t = P.encode(t);
        for (var r = Math.floor(t.length / 2), i = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", n = i.split("").reduce((function(t, e, r) {
            return t[e] = r,
            t
        }
        ), {}), s = Math.floor(64 * Math.random()), a = i[s], o = O(e + a).substr(s % 8, s % 8 + 7).split("").map((function(t) {
            return t.charCodeAt()
        }
        )), l = "", c = 0, u = 0; u < t.length; u++)
            l += i[((c = c == o.length ? 0 : c) + n[t[u]] + o[c++]) % 65],
            r && (l += t.slice(u + 1, u + 1 + r),
            u += r);
        return encodeURIComponent(a + l)
    },
    decode: function(t) {
        var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "appbgzjnopv1917";
        e = O(e);
        var r = 3
          , i = +(t = decodeURIComponent(t)).substr(-r)[1]
          , n = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
          , s = n.split("").reduce((function(t, e, r) {
            return t[e] = r,
            t
        }
        ), {})
          , a = t[0]
          , o = n.indexOf(a)
          , l = O(e + a).substr(o % 8, o % 8 + 7).split("").map((function(t) {
            return t.charCodeAt()
        }
        ));
        t = t.substr(1, t.length - r - 1);
        for (var c = "", u = 0, h = 0, d = 0; d < t.length; d++) {
            for (h = h == l.length ? 0 : h,
            u = s[t[d]] - h - l[h++]; u < 0; )
                u += 65;
            c += n[u],
            i && (c += t.slice(d + 1, d + 1 + i),
            d += i)
        }
        return P.decode(c)
    }
}
N = function(t) {
    e = "app8aplieli4282"
    if ("string" == typeof t && t.startsWith("[xiaoe]")) {
        var r = M && M.decode(t.substring("[xiaoe]".length, t.length), e)
          , i = /#EXT-X-KEY:[^\n]*URI="(.*)"[^\n]*\n/i
          , n = r.match(i);
        return n && "" === n[1] && (r = r.replace(i, "")),
        "string" == typeof r && r.indexOf("#EXTM3U"),
        r
    }
    return t
}