#!/usr/bin/env python
from mp_writer.crew import MpWriterCrew


def run():
    # Replace with your inputs, it will automatically interpolate any tasks and agents information
    inputs = {
        'topic': '什么是crewAI的Agent和Task？',
    }
    MpWriterCrew().crew().kickoff(inputs=inputs)