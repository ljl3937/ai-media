#!/usr/bin/env python
from agent_maker.crew import AgentMakerCrew


def run():
    # Replace with your inputs, it will automatically interpolate any tasks and agents information
    inputs = {
        'topic': 'AI LLMs'
    }
    AgentMakerCrew().crew().kickoff(inputs=inputs)