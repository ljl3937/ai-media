import praw
import time
import os

from langchain.tools import tool
from langchain.llms import Ollama
from crewai import Agent, Task, Process, Crew
from langchain_community.llms import Baichuan

baichuan = Baichuan(baichuan_api_key=os.environ["BAICHUAN_API_KEY"])



from langchain.agents import load_tools

# To load Human in the loop
human_tools = load_tools(["human"])

# To Load GPT-4
api = os.environ.get("OPENAI_API_KEY")


# To Load Local models through Ollama
mistral = Ollama(model="mistral")
gemma = Ollama(model="gemma:2b")


class BrowserTool:
    @tool("Scrape reddit content")
    def scrape_reddit(max_comments_per_post=7):
        """Useful to scrape a reddit content"""
        reddit = praw.Reddit(
            client_id="client-id",
            client_secret="client-secret",
            user_agent="user-agent",
        )
        subreddit = reddit.subreddit("LocalLLaMA")
        scraped_data = []

        for post in subreddit.hot(limit=12):
            post_data = {"title": post.title, "url": post.url, "comments": []}

            try:
                post.comments.replace_more(limit=0)  # Load top-level comments only
                comments = post.comments.list()
                if max_comments_per_post is not None:
                    comments = comments[:7]

                for comment in comments:
                    post_data["comments"].append(comment.body)

                scraped_data.append(post_data)

            except praw.exceptions.APIException as e:
                print(f"API Exception: {e}")
                time.sleep(60)  # Sleep for 1 minute before retrying

        return scraped_data


"""
- define agents that are going to research latest AI tools and write a blog about it 
- explorer will use access to internet and LocalLLama subreddit to get all the latest news
- writer will write drafts 
- critique will provide feedback and make sure that the blog text is engaging and easy to understand
"""

explorer = Agent(
    role="AI高级研究员",
    goal="在2024年的reddit的LocalLLama模块上找到并探索最令人兴奋的项目和公司",
    backstory="""你是一位擅长发现人工智能、科技和机器学习领域新兴趋势和公司的专家策略家。 
    你擅长在LocalLLama subreddit上找到有趣、令人兴奋的项目。你将从reddit的LocalLLama模块上抓取的数据转化为详细的报告，报告中包含了ai/ml领域最令人兴奋的项目和公司的名称。报告中只使用从reddit的LocalLLama模块抓取的数据。
    """,
    verbose=True,
    allow_delegation=False,
    tools=[BrowserTool().scrape_reddit] + human_tools,
    llm=gemma,  # 移除以使用默认的gpt-4
)

writer = Agent(
    role="高级技术作家",
    goal="使用简单的、通俗的词汇，撰写关于最新AI项目的引人入胜和有趣的博客文章",
    backstory="""您是技术创新方面的专家作家，特别是在人工智能和机器学习领域。你知道如何写得引人入胜，有趣但简单，直接和简洁。
    你知道如何用外行人的语言将复杂的专业术语以一种有趣的方式呈现给普通观众。
    只使用从reddit的locallama模块上抓取的数据作为博客。
    请使用中文输出。""",
    verbose=True,
    allow_delegation=True,
    llm=gemma,  # remove to use default gpt-4
)
critic = Agent(
    role="专家写作评论家",
    goal="提供反馈和批评博客文章草稿。确保语气和写作风格引人入胜，简单和简洁",
    backstory="""你是一位擅长向技术作家提供反馈的专家。你能看出博客文章是否简洁、简单或足够吸引人。你知道如何提供有用的反馈，可以改进任何文本。你知道如何确保文本保持技术性和深入，同时使用通俗的术语。
    """,
    verbose=True,
    allow_delegation=True,
    llm=gemma,  # remove to use default gpt-4
)

task_report = Task(
    description="""使用和总结从reddit的locallama版块抓取的数据，对人工智能领域最新的新兴项目做出详细的报告。仅使用从locallama抓取的数据来生成报告。你的最终答案必须是一个完整的分析报告，只有文本，忽略任何代码或任何不是文本的东西。报告必须包含要点，并包含5-10个令人兴奋的新人工智能项目和工具。写下每个工具和项目的名称。每个要点必须包含3句话，涉及一个特定的人工智能公司，产品，模型或任何你在subreddit locallama上找到的东西 
    """,
    agent=explorer,
)

task_blog = Task(
    description="""写一篇只有文字的博客文章，标题简短但有影响力，至少10段。博客应总结在reddit的localLLama板块上发现的最新ai工具的报告。风格和语气应该引人入胜和简洁，有趣，技术性但也使用通俗的词语给普通大众。在AI世界中，具体命名新的，令人兴奋的项目，应用和公司。不要写"**段落 [段落的编号]:**"，而是在新的一行开始新的段落。将项目和工具的名称写成粗体。
    总是包含到项目/工具/研究论文的链接。只包含来自LocalLLAma的信息。请使用中文。
    对于你的输出，使用以下的markdown格式：
    ```
    ## [文章标题](项目链接)
    - 有趣的事实
    - 自己对如何将其与新闻通讯的整体主题联系起来的想法
    ## [第二篇文章的标题](项目链接)
    - 有趣的事实
    - 自己对如何将其与新闻通讯的整体主题联系起来的想法
    ```
    """,
    agent=writer,
)

task_critique = Task(
    description="""输出必须有以下的markdown格式：
    ```
    ## [文章标题](项目链接)
    - 有趣的事实
    - 自己对如何将其与新闻通讯的整体主题联系起来的想法
    ## [第二篇文章的标题](项目链接)
    - 有趣的事实
    - 自己对如何将其与新闻通讯的整体主题联系起来的想法
    ```
    确保它是这样的，如果不是，相应地重写它。
    """,
    agent=critic,
)

# instantiate crew of agents
crew = Crew(
    agents=[explorer, writer, critic],
    tasks=[task_report, task_blog, task_critique],
    verbose=2,
    process=Process.sequential,  # Sequential process will have tasks executed one after the other and the outcome of the previous one is passed as extra content into this next.
)

# Get your crew to work!
result = crew.kickoff()

print("######################")
print(result)