from langchain_openai import ChatOpenAI
from langchain.chains import LLMRequestsChain, LLMChain
from langchain.prompts import PromptTemplate
import os

llm = ChatOpenAI(temperature=0, model_name='gpt-3.5-turbo', openai_api_key=os.environ["OPENAI_API_KEY"])
template = """在 >>> 和 <<< 之间是网页的返回的HTML内容。

网页是AI破局的精华文章的索引网站

请根据这个网页的 html，用python写一个用来抓取这个网页所有文章标题和 url 的爬虫

>>> {requests_result} <<<
"""

PROMPT = PromptTemplate(
    input_variables=["requests_result"],
    template=template,
)
chain = LLMRequestsChain(llm_chain = LLMChain(llm=llm, prompt=PROMPT))
inputs = {
    "url": "https://ai.ifindyi.com/#/"
}
response = chain(inputs)
print(response['output'])
