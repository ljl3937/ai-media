from langchain_community.agent_toolkits import PlayWrightBrowserToolkit
from langchain_community.llms import Baichuan
from langchain_community.tools.playwright.utils import (
    create_async_playwright_browser,  # A synchronous browser is available, though it isn't compatible with jupyter.\n",      },
)
import os

from langchain_openai import ChatOpenAI

llm = Baichuan()

async_browser = create_async_playwright_browser()
toolkit = PlayWrightBrowserToolkit.from_browser(async_browser=async_browser)
tools = toolkit.get_tools()
print(tools)

from langchain.agents import initialize_agent, AgentType

# LLM不稳定，对于这个任务，可能要多跑几次才能得到正确结果
# llm = ChatOpenAI(temperature=0.5)  

agent_chain = initialize_agent(
    tools,
    llm,
    agent=AgentType.STRUCTURED_CHAT_ZERO_SHOT_REACT_DESCRIPTION,
    verbose=True,
)

async def main():
    response = await agent_chain.arun("帮我获取https://www.toutiao.com中前10个标题?")
    print(response)

import asyncio
loop = asyncio.get_event_loop()
loop.run_until_complete(main())